# LDV Datamanagement Task

A set of utils that can be used to manage the tasks data in ATDB



## Installation

To install the package go into the code repository and 
execute:
```
pip install .
```

## Usage
```
atdb_mng clean --workflow [workflow_id] --status [tasks_status] 
```

## Contributing

To contribute, please create a feature branch and a "Draft" merge request.
Upon completion, the merge request should be marked as ready and a reviewer
should be assigned.

Verify your changes locally and be sure to add tests. Verifying local
changes is done through `tox`.

```pip install tox```

With tox the same jobs as run on the CI/CD pipeline can be ran. These
include unit tests and linting.

```tox```

To automatically apply most suggested linting changes execute:

```tox -e format```

## License
This project is licensed under the Apache License Version 2.0
