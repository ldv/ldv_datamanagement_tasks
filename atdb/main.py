"""
Main entry point for command line script
"""

import logging
import os
from argparse import ArgumentParser, Namespace
from configparser import ConfigParser

from atdb.prune import prune
from atdb.fix import fix

DEFAULT_PATH = os.path.expanduser("~/.config/ldv/services.cfg")
logging.basicConfig(
    level=logging.DEBUG, format="%(asctime)s %(levelname)s : %(message)s"
)

logger = logging.getLogger("atdb_mngr")


def read_conf_file(args: Namespace, additional_location=None):
    """
    Reads configuration files and append results to args namespace
    """
    parser = ConfigParser()
    if additional_location is not None:
        read_files = parser.read(additional_location)
    else:
        read_files = parser.read(DEFAULT_PATH)

    if not read_files and (args.atdb_site is None or args.token is None):
        raise SystemError("Missing configuration file")
    global_config = parser["ATDB"]
    if "url" in global_config:
        args.atdb_site = parser["ATDB"]["url"]
    if "token" in global_config:
        args.token = parser["ATDB"]["token"]
    return args


def parse_args() -> (Namespace, ArgumentParser):
    """
    Parse command line arguments
    """
    parser = ArgumentParser(description="ATDB management tool")
    parser.add_argument(
        "--atdb_site", help="ATDB url", default="https://sdc-dev.astron.nl:5554/atdb"
    )
    parser.add_argument(
        "--token",
        help="ATDB token (if not provided it reads it from file",
    )
    parser.add_argument(
        "--config",
        help="Configuration file "
        "(tries to load the one at ~/.config/ldv/services.cfg if not specified)",
        default=None,
    )
    parser.add_argument("--dry_run", help="Test execution", action="store_true")
    parser.add_argument("-v", action="store_true")
    subparser = parser.add_subparsers(help="commands", dest="operation")
    prune_parser = subparser.add_parser("prune")
    prune_parser.add_argument("--workflow_id", help="Filters by workflow id")
    prune_parser.add_argument("--status", help="Filter by status")

    _ = subparser.add_parser("fix")

    return parser.parse_args(), parser


def main():
    """
    Main entry point
    """
    args, parser = parse_args()
    args = read_conf_file(args, args.config)
    if args.v:
        logger.setLevel(logging.DEBUG)

    if args.operation == "prune":
        prune(args)
    elif args.operation == "fix":
        fix(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
