"""
Test prune command
"""
from unittest import TestCase
from atdb.prune import extract_surls_from_obj


class TestPruneUtils(TestCase):
    """Test Case of the prune utility functions"""

    def test_surl_filtering(self):
        """
        Test surl filtering utility function
        """
        test_data = {
            "item": [{"surl": "onesurl"}, 1, ["ciao", {"surl": "another_surl"}]],
            "item2": {"surl": "third_surl"},
            "item3": "bla",
        }
        result = extract_surls_from_obj(test_data)
        expected_result = ["onesurl", "another_surl", "third_surl"]
        self.assertListEqual(result, expected_result)
